#!/usr/bin/env python
#
# get_strain_info_from_genbank_fasta.py

"""get_strain_info_from_genbank_fasta.py  last modified 2022-10-10

    select all fasta-format genome assemblies files
    creates a tab-delimited file of assembly ID to strain names, such as:

ASM5606v1	Lactobacillus delbrueckii bulgaricus ATCC11842

./get_strain_info_from_genbank_fasta.py ncbi-refseq-2022-01-11/*.fna.gz > lactobacillus_assembly_to_id.tab

"""

import sys
import re
import gzip
import time
import argparse
from collections import defaultdict,Counter
from Bio import SeqIO

def read_all_nucl_fasta_gz(fastafilelist):
	'''read all fasta files, get some stats'''

	assembly_id_dict = {} # key is assembly ID, like ASM1198v1, value is True

	assembly_to_long_name = {}

	unique_accessions = defaultdict(int) # key is accession, first split, value is count

	for fastafilename in fastafilelist:

		try:
			assembly_id = re.search("\.\d_(.*)_genomic\.fna", fastafilename).group(1)
		except AttributeError:
			print( "ERROR: cannot parse assembly ID from {}".format(fastafilename) , file=sys.stderr)
		if assembly_id in assembly_id_dict:
			print( "WARNING: duplicate assembly ID {} from {}".format(assembly_id, fastafilename), file=sys.stderr)
			#continue
		else:
			assembly_id_dict[assembly_id] = True

		# check for gzip
		if fastafilename.rsplit('.',1)[-1]=="gz": # autodetect gzip format
			_opentype = gzip.open
	#		sys.stderr.write("# Opening {} as gzipped\n".format(fastafilename))
		else: # otherwise assume normal open for fasta format
			_opentype = open
	#	sys.stderr.write("# Reading {}\n".format(fastafilename))

		long_name = None
		seqcounter = 0 # reset each file
		# iterate seqs
		with _opentype(fastafilename,'rt') as fa:
			for seqrec in SeqIO.parse(fa,"fasta"):
				seqcounter += 1
				seqid = seqrec.id
				unique_accessions[seqid] += 1

				seqdesc = seqrec.description.split(" ",1)[1].rsplit(",",1)[0] # usually full line, unparsed
				#NZ_AP019750.1 Lactobacillus delbrueckii subsp. delbrueckii strain NBRC 3202 chromosome, complete genome

				# ignore sequences designated as plasmid
				if seqdesc.find("plasmid")>-1:
					continue

				# overwrites until last entry in fasta file
				long_name = seqdesc.replace("strain ","").replace("subsp. ","").replace(" chromosome","").replace("sp.","sp")
				long_name = long_name.split("mitochondri",1)[0]
				long_name = long_name.split("unassigned",1)[0]
				long_name = long_name.replace("complete sequence","")
				long_name = long_name.replace("ATCC ","ATCC").replace("NBRC ","NBRC")
				long_name = long_name.replace(".","-").replace("#","-").replace("(","-").replace(")","").split("=")[0].strip()

		assembly_w_long_name = "{}\t{}\n".format( assembly_id, long_name)
		sys.stdout.write(assembly_w_long_name)
		sys.stderr.write("# {}  {}  {} \n".format( seqcounter, assembly_id, long_name ))


def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_fasta', nargs="*", help="fasta format files, can be .gz")
	args = parser.parse_args(argv)

	read_all_nucl_fasta_gz(args.input_fasta)


if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
