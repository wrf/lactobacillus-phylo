#!/usr/bin/env python
#
# add_gene_names_to_matrix.py

"""add_gene_names_to_matrix.py  last modified 2022-07-07

add_gene_names_to_matrix.py

"""

import sys
import argparse

def parse_proteins(proteinfile):
	"""read fasta file of NCBI proteins, return a dict where key is seq ID and value is gene description"""
	line_counter = 0
	prot_counter = 0
	name_to_description = {}
	for line in open(proteinfile, 'r'):
		line = line.strip()
		if line and line[0]!="#": # skip empty lines or comments
			prot_counter += 1
			if line[0]==">": # meaning header line
				# >WP_002876384.1 MULTISPECIES: S-adenosyl-l-methionine hydroxide adenosyltransferase family protein [Lactobacillus]
				# >Lactobacillus_delbrueckii_lactis_DSM_20072|2  WP_002876384.1
				seqsplits = line.split(" ",1)
				seq_id = 
				description = .replace("MULTISPECIES:","").strip



def main(argv, wayout):
	if not len(argv):
		argv.append('-h')
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-g','--combine-genes', action="store_true", help="use long RAxML type partition format")
	parser.add_argument('-m','--matrix', help="gene matrix from check_supermatrix_alignments.py ")
	parser.add_argument('-p','--proteins', help="downloaded genbank proteins, as fasta")
	parser.add_argument('-r','--renamed-proteins', help="renamed proteins, as fasta")
	args = parser.parse_args(argv)

	new_name_to_old_name = {}
	old_name_to_description = parse_raw_proteins(args.proteins)













if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
