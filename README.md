# Lactobacillus phylogeny #
Modeled after [Zheng 2015](https://doi.org/10.1128/AEM.02116-15) and [Zheng 2020](https://doi.org/10.1099/ijsem.0.004107), though they did not make their raw data available! They do provide a link to some of the info in the second paper, at [https://site.unibo.it/subcommittee-lactobacillus-bifidobacterium/en](https://site.unibo.it/subcommittee-lactobacillus-bifidobacterium/en) . This link is given in the paper as `https://site.unibo.it/subcommittee‐lactobacillus‐bifidobacterium/en` containing the alternate unicode `U+2010 HYPHEN` that does not work, instead of the normal `U+002D HYPHEN-MINUS`.

## downloading and tidying data ##
Starting from [NCBI Taxonomy of *Lactobacillus*](https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=1578&mode=info), click on the link for [Assembly](https://www.ncbi.nlm.nih.gov/assembly/?term=txid1578[Organism:exp])

On the left side, filter by **Assembly level** for "Complete genome", then click **Download Assemblies** from RefSeq as "Protein FASTA (.faa)". The `.tar` zip contains a folder called `ncbi-genomes` with `-YYYY-MM-DD`, and all protein files inside are zipped with `.gz`. The genomic nucleotides are also downloaded in the same way.

Many of the proteins do not include strain information in the species ID; the one below should be `Lactobacillus delbrueckii bulgaricus DSM 20080`:

```
gzip -dc GCF_001953135.1_ASM195313v1_protein.faa.gz | head
>WP_002876367.1 GntR family transcriptional regulator [Lactobacillus delbrueckii]
MKFKDNLPIYLQIEDYIYLQIAQGKLQAGEKLPSVRALALELTVNANTVQRALREMTAKGYIFTKRGEGNFVTTDEGRLE
EMKSKLLKQELAAFVSRMEKLGVERENLTGLLASYLDDTREE
```

...so these strain IDs need to be extracted from the genome in another way.

`./get_strain_info_from_genbank_fasta.py ncbi-completed-2022-01-11/*.fna.gz > lactobacillus_assembly_to_id.tab`

```
./filter_and_rename_genbank_prots.py -n lactobacillus_assembly_to_id.tab ncbi*/*.faa.gz`
mkdir lactobacillus_renamed/
mv ../ncbi-*-2022-01-11/*renamed.faa ./
cat lactobacillus_renamed/*.faa > lactobacillus_all_proteins.fasta
```

In total, 277 species/strains are used, encoding a total of 530223 proteins (160561051 AAs, average 302 AAs). The main reference strain would be [Lactobacillus delbrueckii subsp. bulgaricus ATCC 11842](https://www.ncbi.nlm.nih.gov/assembly/GCF_000056065.1/).

## making orthologous protein groups ##
Then, using [DIAMOND](https://github.com/bbuchfink/diamond), align all proteins against all others, which takes around 20 minutes on my laptop. Because there are over 200 species, the `-k` option must be changed, which is by default 25 for diamond (default for blastp is 500).

```
~/diamond-v2.0.13/diamond-v2.0.13 makedb -d lactobacillus_all_proteins.fasta --in lactobacillus_all_proteins.fasta
~/diamond-v2.0.13/diamond-v2.0.13 blastp -k 250 -q lactobacillus_all_proteins.fasta -d lactobacillus_all_proteins.fasta -o lactobacillus_all_proteins.all_v_all.k250.tab
```

Run the MCL clustering, and filter with an old script [makehomologs.py](https://bitbucket.org/wrf/sequences/src/master/makehomologs.py):

`makehomologs.py -i lactobacillus_all_proteins.all_v_all.k250.tab -f lactobacillus_all_proteins.fasta -p 234 -M 700 -T 4 -b 0.5 -o lactobacillus_v4`

Stats for the orthogroups are plotted with the automated plotter [plot_homolog_output_logs.R](https://github.com/wrf/supermatrix/blob/master/plot_homolog_output_logs.R) in the [supermatrix repo](https://github.com/wrf/supermatrix). Across the 277 species, the largest single-copy orthogroup contains 277 taxa, with a peak nearby for one or two missing taxa. Otherwise, like many bacterial groups, gene content can be highly variable between clades, or in this case, species or subspecies.

`Rscript ~/git/supermatrix/plot_homolog_output_logs.R lactobacillus_v4.2022-05-16-111140.mh.log fasta_clusters.lactobacillus_v4.tab`

For all plots, dark blue refers to clusters of single-copy orthologs, meaning at most 1 copy per taxon.

![lactobacillus_v4.2022-05-16-111140.mh1.png](https://bitbucket.org/wrf/lactobacillus-phylo/raw/e771e17132b00cf4738b44eafa44af33309e9d89/images/lactobacillus_v4.2022-05-16-111140.mh1.png)

The pattern below differs strongly with many eukaryotic clusters, in which protein families could have dozens or hundreds of homologs in each species. This somewhat depends on how small the clusters are refined (using the inflation parameter `-i` in `mcl` or `makehomologs.py`), since even within a mammalian family, multi-copy genes may still appear to be single copy relative to that family.

![lactobacillus_v4.2022-05-16-111140.mh2.png](https://bitbucket.org/wrf/lactobacillus-phylo/raw/e771e17132b00cf4738b44eafa44af33309e9d89/images/lactobacillus_v4.2022-05-16-111140.mh2.png)

![lactobacillus_v4.2022-05-16-111140.mh3.png](https://bitbucket.org/wrf/lactobacillus-phylo/raw/e771e17132b00cf4738b44eafa44af33309e9d89/images/lactobacillus_v4.2022-05-16-111140.mh3.png)

## aligning and tree building ##
The arrangment of species was similarly recovered by [Zheng 2015](https://doi.org/10.1128/AEM.02116-15), but not by [France 2016](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5118917/) that found *L. delbrueckii* as sister to remaining true *Lactobacillus*.

![lactobacillus_homologs_999_combined_mat.tree.png](https://bitbucket.org/wrf/lactobacillus-phylo/raw/fd32f52b5bd77298176316f1c12691471079fb75/images/lactobacillus_homologs_999_combined_mat.tree.png)

Align the clusters with [MAFFT](https://mafft.cbrc.jp/alignment/software/), which can be done across all clusters with a for-loop-in-the-shell.

`for FILE in *.fasta ; do BASE="${FILE%.fasta}" ; mafft $FILE > $BASE.aln ; done`

Merge the alignments into a supermatrix with [join_alignments.py](https://github.com/wrf/supermatrix/blob/master/join_alignments.py), here the wildcard should take any that are numbered under 00NNN, up to 999, but actually including only 240 genes.

`~/git/supermatrix/join_alignments.py -a clusters_lactobacillus_v4/homologs_lactobacillus_v4_00*aln -d "|" -u lactobacillus_homologs_999_combined_mat.aln`

`~/git/supermatrix/check_supermatrix_alignments.py -a lactobacillus_homologs_999_combined_mat.aln -p lactobacillus_homologs_999_combined_mat.aln.partition.txt --percent -m lactobacillus_homologs_999_combined_mat.matrix.tab`

`Rscript ~/git/supermatrix/draw_matrix_occupancy.R lactobacillus_homologs_999_combined_mat.matrix.tab`

Tree is rapidly inferred using [FastTreeMP](http://www.microbesonline.org/fasttree/):

`~/fasttree/FastTreeMP_2.1.11 lactobacillus_homologs_999_combined_mat.aln > lactobacillus_homologs_999_combined_mat.tree`

This is converted into `.nex` format, and then reused to make the occupancy matrix in the same order as the tree.

`~/git/supermatrix/check_supermatrix_alignments.py -a lactobacillus_homologs_999_combined_mat.aln -p lactobacillus_homologs_999_combined_mat.aln.partition.txt --percent -m lactobacillus_homologs_999_combined_mat.matrix.tab -T lactobacillus_homologs_999_combined_mat.nex`

`Rscript ~/git/supermatrix/draw_matrix_occupancy.R lactobacillus_homologs_999_combined_mat.matrix.tab`

## notes on species and metabolism ##
### species diversity ###
The identities of common microbiome inhabitants have changed over the years, and this is now understood to be [L. crispatus](https://en.wikipedia.org/wiki/Lactobacillus_crispatus), [L. jensenii](https://en.wikipedia.org/wiki/Lactobacillus_jensenii), [L. gasseri](https://en.wikipedia.org/wiki/Lactobacillus_gasseri), and [L. iners](https://en.wikipedia.org/wiki/Lactobacillus_iners). ([Ravel 2011](https://pubmed.ncbi.nlm.nih.gov/20534435/))

> "Lactobacillus acidophilus has been reported to be the predominant vaginal species. Vaginal lactobacilli isolated from 215 sexually active women were identified using whole-chromosomal DNA probes to 20 American Type Culture Collection Lactobacillus strains. Most women were colonized by L. crispatus (32%), followed by L. jensenii (23%), a previously undescribed species designated L. 1086V (15%), L. gasseri (5%), L. fermentum (0.3%), L. oris (0.3%), L. reuteri (0.3%), L. ruminis (0.3%), and L. vaginalis (0.3%). H2O2 was produced by 95% of L. crispatus and 94% of L. jensenii isolates, compared with only 9% of L. 1086V. Colonization by L. crispatus or L. jensenii was positively associated with being white (P<.001), age >/=20 years (P=.05), barrier contraceptive usage (P=.008), and lower frequency of bacterial vaginosis (P<.001) and gonorrhea (P=.03). L. crispatus and L. jensenii, not L. acidophilus, are the most common species of vaginal lactobacilli." ([Antonio 1999](https://pubmed.ncbi.nlm.nih.gov/10558952/))

> "Species of the Lactobacillus acidophilus complex are generally considered to constitute most of the vaginal Lactobacillus flora, but the flora varies between studies. ... The vaginal flora of most participants was dominated by a single RAPD type, but five of them harbored two RAPD types representing two different species or strains. The most frequently occurring species were Lactobacillus crispatus, Lactobacillus gasseri, Lactobacillus iners, and Lactobacillus jensenii. L. iners has not previously been reported as one of the predominant Lactobacillus species in the vagina." ([Vasquez et al 2002](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC120688/))

There still appears to be some uncertainty about defining *Mycoplasma* and *Ureaplasma* as pathogens, as though the researchers fail to understand that an opportunistic pathogen is a pathogen. Many viruses, like covid, or UTI pathogens, like Chlamydia, do not present symptoms in all people, but that does not mean it is somehow "commensal". Both *Mycoplasma* and *Ureaplasma* were in the supplemental heatmap figure S1 of [Ravel 2011](https://pubmed.ncbi.nlm.nih.gov/20534435/), found mostly in CST-IV, but not displayed in the main heatmap figure 1, and not mentioned at all in the paper.

> "Mycoplasma and Ureaplasma are commonly reported vaginal constituents, further supporting the suggestion that these organisms are part of the vaginal microbiota of many clinically healthy women." ([Chaban 2014](https://pmc.ncbi.nlm.nih.gov/articles/pmid/25053998/))

> "Mollicutes were detected in vaginal samples of 217/310 (70%) women, with 149/310 (48%) testing positive for Ureaplasma spp. U. parvum was detected more frequently (127/310 = 41%) than U. urealyticum (21/310 = 7%)." ([Albert 2015](https://doi.org/10.1371/journal.pone.0135620))

> "The L. iners group showed a relationship only with the genus Ureaplasma (r = 0.20). This was in contrast to what we expected on the basis of its small genome size that could suggest it needs metabolic cooperation with other vaginal bacteria to compensate for its lack of specific relevant functions." ([Lebeer 2023](https://pmc.ncbi.nlm.nih.gov/articles/PMC10627828/))

### ecological stable states ###
The four main species are found at various amounts, some women with a mix. This seems to require an ecological explanation. One possibility is that microbiomes must be self-reinforcing, so the species have various ability to resist change.

> "L. crispatus was present in 39 respectively 44 women with grade I VMF during the first respectively second trimester. When accounting for the first-to-second and second-to-third trimester transitions respectively, L. crispatus disappeared twice (5.1%) respectively once (2.3%).... compared to L. crispatus, L. gasseri and/or iners were found to be significantly less stable microflora components (McNemar odds ratio 23.33, 95% CI 7.10 – 92.69, p < 0.001)." ([Verstraelen et al 2009](https://pmc.ncbi.nlm.nih.gov/articles/PMC2698831/))

Another explanation is that the four species have generally different tolerance to pH, or other factors, so the balance is the inevitable result of a dynamic habitat. This was found by [Navarro 2023](https://doi.org/10.1186/s12866-023-02916-8), noting different pH tolerance and requirement of glycogen:

> "While MSVF supported the growth of all four species for 30 d in this study, the viability of each species was dependent on pH: L. crispatus survived for 30 d only at pH 5.0; L. gasseri and G. vaginalis survived for 30 d at pH 4.5 and 5.0 but not at pH 4.0; and L. jensenii survived for 30 d at pH 4.0, 4.5, and 5.0... it is possible that at pH 5.0 in NYCB, G. vaginalis metabolizes the glucose and/or the peptone in the medium to produce acetate causing the pH level to drop, thus eliminating their growth. Whether acetate production is the cause of the pH drop or not, the death of the G. vaginalis cultures suggests that pH 4.5 is the lower limit for its survival, which may explain why it is not present in L. crispatus-dominated CST I with a pH range of 3.7–4.3 in vivo... our results show that, not only is glycogen essential for the growth of L. jensenii 62G, L. gasseri 63 AM, and L. crispatus JV-V01 and G. vaginalis JCP8151A, but they also suggest that the pH of the medium influences their viability by affecting their ability to metabolize glycogen... Each CST of the vaginal microbiota is associated with a specific pH range of the vaginal fluid, with the pH of CST I ranging from 3.7 to 4.3, that of CST II ranging from 4.3 to 5.7, CST III from pH 3.8 to 5.0, CST IV containing G. vaginalis ranging from pH 4.7 to 5.9, and CST V from 4.3 to 5.1" ([Navarro 2023](https://doi.org/10.1186/s12866-023-02916-8))

It is also possible that the four main species cannot and should not outcompete one another, thereby increasing the pan-genome of the microbial populations. This was also weakly the argument of [Ma 2020](https://pmc.ncbi.nlm.nih.gov/articles/PMC7044274/), though their data indicate that the four main species do not have substantial strain variation (in their Figure 3) as compared to Gardnerella or Atopobium. Very few key genes may be mediating the probiotic properties, e.g. bacteriocins. Another study argues this:

> "Our results provide evidence of strain-level specificity for certain antimicrobial properties among cervicovaginal L. gasseri and L. crispatus strains, indicating that the presence of a particular species in the vaginal microbiota is not sufficient to determine its benefit to the host. A full repertory of antimicrobial properties should be evaluated in choosing vaginal microbiota-associated Lactobacillus isolates for the development of live biotherapeutic strategies." ([Atassi 2019](https://pmc.ncbi.nlm.nih.gov/articles/PMC6933176/))

### use as probiotic for women ###
Different species have widely variable amounts of research:

* [Lactobacillus crispatus probiotic](https://pubmed.ncbi.nlm.nih.gov/?term=lactobacillus+plantarum+probiotic)  =  197
* [Lactobacillus iners probiotic](https://pubmed.ncbi.nlm.nih.gov/?term=lactobacillus+iners+probiotic)  =  30
* [Lactobacillus gasseri probiotic](https://pubmed.ncbi.nlm.nih.gov/?term=lactobacillus+gasseri+probiotic)  =  319
* [Lactobacillus jensenii probiotic](https://pubmed.ncbi.nlm.nih.gov/?term=lactobacillus+jensenii+probiotic)  =  31
* [Lactobacillus delbrueckii probiotic](https://pubmed.ncbi.nlm.nih.gov/?term=lactobacillus+delbrueckii+probiotic)  =  312
* [Lactobacillus bulgaricus probiotic](https://pubmed.ncbi.nlm.nih.gov/?term=lactobacillus+bulgaricus+probiotic)  =  419
* [Lactobacillus acidophilus probiotic](https://pubmed.ncbi.nlm.nih.gov/?term=lactobacillus+acidophilus+probiotic)  =  1387
* [Lactobacillus rhamnosus probiotic](https://pubmed.ncbi.nlm.nih.gov/?term=lactobacillus+rhamnosus+probiotic)  =  1985
* [Lactobacillus reuteri probiotic](https://pubmed.ncbi.nlm.nih.gov/?term=lactobacillus+reuteri+probiotic)  =  978
* [Lactobacillus plantarum probiotic](https://pubmed.ncbi.nlm.nih.gov/?term=lactobacillus+plantarum+probiotic)  =  3741

Many of the clinical trials seem to involve oral administration of Lactobacillus to cure vaginal infections, which makes very little sense, including [Reid 2001](https://pubmed.ncbi.nlm.nih.gov/11750220/) using Lactobacillus rhamnosus GR-1 plus Lactobacillus fermentum RC-14 probiotic dosage regimens or L. rhamnosus GG by mouth each day for 28 days, [Reid 2003](https://pubmed.ncbi.nlm.nih.gov/12628548/) with Lactobacillus rhamnosus GR-1 and Lactobacillus fermentum RC-14 for 60 days, [Qi 2023](https://pmc.ncbi.nlm.nih.gov/articles/PMC10415204/) with oral Lactobacillus gasseri TM13 and Lactobacillus crispatus LG55 as an adjuvant to MET for 30 days.

This one did not [Vitali 2016](https://pubmed.ncbi.nlm.nih.gov/26572459/), instead trying Lactobacillus crispatus BC5 in some other kind of insert, not photographed or described in the paper.

Bacteriocin genes were identified in the strains by PCR, for L. gasseri 1A‐TV ([Acidocin A](https://www.ncbi.nlm.nih.gov/protein/BAA07120.1) 81AAs, [Helveticin J](https://www.ncbi.nlm.nih.gov/protein/AAA63274.1) 333AAs), L. fermentum 18A‐TV (none detected), and L. crispatus 35A‐TV (Acidocin A). ([Scillato et al 2021](https://pmc.ncbi.nlm.nih.gov/articles/PMC8483400/))

> "The antagonistic activity of L. gasseri 1A‐TV, L. fermentum 18A‐TV, and L. crispatus 35A‐TV, assessed by the agar spot test, showed the best growth inhibition with diameters >10 mm (+ + + +) for E. coli GM1, S. aureus SA3, E. faecalis EFS1, S. agalactiae GB022, E. faecium 75 VRE and for K. pneumoniae 340 KPC, multidrug‐resistant pathogens frequently associated with serious infections. All three Lactobacilli antagonized C. albicans, showing inhibition zones between 6 and 10 mm (+ + +), and exerted a partial inhibition versus C. glabrata, with inhibition zones between 1 and 3 mm. They also showed good inhibition versus P. aeruginosa, P. mirabilis, and P. vulgaris with diameters between 3 and 6 mm (++)... our Lactobacilli possessed strong biofilm formation capacity when tested in combination; however, they were poor producers when tested alone. These data make us hypothesize a synergistic interspecific interaction between our Lactobacilli to optimize their living conditions." ([Scillato et al 2021](https://pmc.ncbi.nlm.nih.gov/articles/PMC8483400/))

Another study isolated [17 strains](https://www.ncbi.nlm.nih.gov/nuccore/AB976542) from women. Effectively all 8 of their isolated L. crispatus strains were fungistatic in vitro, and most were fungicidal against Candida albicans. Four of the L. crispatus strains also showed substantial adhesion abilities to HeLa cells. 

> "The isolates were taxonomically identified to species level (L. crispatus B1-BC8, L. gasseri BC9-BC14 and L. vaginalis BC15-BC17) by sequencing the 16S rRNA genes. All strains produced hydrogen peroxide and lactate. Fungistatic and fungicidal activities against C. albicans, C. glabrata, C. krusei, C. tropicalis, C. parapsilosis and C. lusitaniae were evaluated by broth micro-dilution method. The broadest spectrum of activity was observed for L. crispatus BC1, BC4, BC5 and L. vaginalis BC15, demonstrating fungicidal activity against all isolates of C. albicans and C. lusitaniae... In particular, L. crispatus BC2, L. gasseri BC10 and L. gasseri BC11 appeared to be the most active strains in reducing pathogen adhesion, as their effects were mediated by both cells and supernatants." ([Parolin et al 2015](https://pmc.ncbi.nlm.nih.gov/articles/PMC4476673/)) 

The same 17 strains were investigated against *Chlamydia trachomatis*, with L. crispatus strains BC1, BC2, BC4, BC6, BC7, BC8, and L. gasseri BC13 showing high activity. ([Nardini 2016](https://pmc.ncbi.nlm.nih.gov/articles/PMC4926251/)) L. crispatus BC4 was then even used in a probiotic cheese. ([Patrignani 2019](https://pmc.ncbi.nlm.nih.gov/articles/PMC6326422/))

Different CSTs were found to inhibit infection of ex vivo vaginal epithelial cells by HIV, with the strongest effect from L. iners-dominated bacterial communities. ([Pyles 2014](https://pmc.ncbi.nlm.nih.gov/articles/PMC3968159/)) The biology of L. iners is not well understood, reviewed by [Zheng 2021](https://pmc.ncbi.nlm.nih.gov/articles/PMC8645935/). Some very imprecise metatranscriptomics was done on L. iners, without controlling for strain, or sequencing the metagnomes of the patients. ([Macklaim 2013](https://pmc.ncbi.nlm.nih.gov/articles/PMC3971606/))

Different CSTs were associated with differential effectiveness of anti-retrovial drug [tenofovir](https://en.wikipedia.org/wiki/Tenofovir_disoproxil), the worst being CST-IV, likely due to metabolism of the drug by Gardnerella.

> "We found that tenofovir concentrations in culture with G. vaginalis decreased rapidly by 50.6% compared with marginal changes in either L. iners (P = 0.0037), L. crispatus (P = 0.0019), or abiotic (same NYCIII media without bacteria) control (P < 0.0001) at 4 hours... G. vaginalis used here was a subtype C strain (ATCC type strain 14018), but repeating these methods with G. vaginalis with Kenyan clinical isolates from three different subtypes demonstrated that all subtypes of G. vaginalis metabolized tenofovir (P < 0.005). Concomitantly to tenofovir loss, intracellular tenofovir concentrations rose sharply in G. vaginalis but not in L. iners or L. crispatus cultures. Predicted metabolites at mass/charge ratios (m/z) of 136.06, 206.10, 159.07, and 59.05 showed a sharp increase at 136.06 m/z in G. vaginalis cultures, indicating adenine production via cleavage of oxy-methylphosphonic acid (Fig. 3C), the side-chain component of tenofovir... The efficacy of tenofovir-containing topical microbicide to prevent HIV infection varied more than threefold depending on vaginal bacterial profiles; tenofovir gel reduced HIV incidence by 61% in LD women but only by 18% in non-LD women." ([Klatt 2017](https://pubmed.ncbi.nlm.nih.gov/28572388/))

Two papers show an unappreciated consequence of effects of the probiotics on sperm viability, [Wang 2019](https://pmc.ncbi.nlm.nih.gov/articles/PMC6909777/) and [Li 2021](https://pmc.ncbi.nlm.nih.gov/articles/PMC8414900/).

> "Isolation of bacteria from vaginal fluid yielded the following species: Staphylococcus epidermidis, L. crispatus, L. acidophilus, L. salivarius, L. helveticus, Pseudomonas stutzeri, Staphylococcus capitis, L. gasseri and Enterococcus faecalis. Of note, all of the tested bacteria, whether probiotic or pathogenic, adhered to sperm in large numbers... nearly all tested bacteria caused a statistically significant reduction in all sperm motility parameters." ([Wang 2019](https://pmc.ncbi.nlm.nih.gov/articles/PMC6909777/))

### use as probiotic for babies ###
> "Strains of coliforms were isolated from stools of 45 colicky and 42 control breastfed infants in McConkey Agar and identified using PCR... The following strains were identified: Escherichia coli, Klebsiella pneumoniae, Klebsiella oxytoca, Enterobacter aerogenes, Enterobacter cloacae and Enterococcus faecalis. Then, 27 Lactobacillus strains were tested for their antagonistic effect against coliforms both by halo-forming method and in liquid co-cultures. [Lactobacillus delbrueckii subsp.delbrueckii DSM20074 = JCM1012](https://www.dsmz.de/collection/catalogue/details/culture/DSM-20074) and L. plantarum MB456 were able to inhibit all coliforms strains (halo-forming method), also in liquid co-cultures..." [Savino et al 2011](https://pmc.ncbi.nlm.nih.gov/articles/PMC3224137/). 

This study, by chance, examined only one of the 4 woman-associated species, *L. gasseri* MB335, but not the other three, and had better results from *Lactobacillus delbrueckii* DSM20074. Also, *L. plantarum* was renamed to [Lactiplantibacillus plantarum](https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=1590), so like many probiotics, no longer is classified as *Lactobacillus*.

Here below as well for several studies, L. reuteri was reassigned to the genus [*Limosilactobacillus*](https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=1598) in [Zheng 2020](https://doi.org/10.1099/ijsem.0.004107), again bringing forth that *L. reuteri* is not that close to the human-associated species of true *Lactobacillus*. The new genus is stated as "Limosilactobacillus - limosus, slimy, referring to the property of most strains in the genus to produce exopolysaccharides from sucrose." Type strain is [Limosilactobacillus reuteri subsp. reuteri F275](https://www.ncbi.nlm.nih.gov/datasets/taxonomy/299033/), aka. [DSM20016](https://www.dsmz.de/collection/catalogue/details/culture/DSM-20016). The strains LTH2584, TMW1.656, and TMW1.112 make a PKS antibiotic [Reutericyclin](https://en.wikipedia.org/wiki/Reutericyclin) "an N-acylated tetramic acid with bacteriostatic and bactericidal activity against Gram-positive bacteria, including Staphylococcus aureus, Listeria innocua, Enterococcus faecium, Clostridium difficile, and bacilli that cause ropy spoilage of bread." ([Lin 2015](https://pmc.ncbi.nlm.nih.gov/articles/PMC4345372/))

The results of clinical trials for use as a probiotic for colic are mixed. In [Savino 2010](https://pubmed.ncbi.nlm.nih.gov/20713478/), crying time decreased from 370 minutes to 30 minutes for the treatment. Even in the placebo group, crying time decreased from 300 minutes to 90 minutes in 21 days, however the rate of change was different: for the treatment group, crying was at 90 minutes at 1 week, and 185 minutes in the placebo group.

> "During the study, there was a significant increase in fecal lactobacilli (P=.002) and a reduction in fecal Escherichia coli and ammonia in the L reuteri group only (P=.001)... Only exclusively breastfed infants were enrolled to prevent variability in the intestinal microbiota caused by diet." ([Savino 2010](https://pubmed.ncbi.nlm.nih.gov/20713478/)) 

Compared to:

> "L reuteri DSM 17938 did not benefit a community sample of breastfed infants and formula fed infants with colic. These findings differ from previous smaller trials of selected populations and do not support a general recommendation for the use of probiotics to treat colic in infants." ([Sung 2014](https://pmc.ncbi.nlm.nih.gov/articles/PMC3972414/)) 

This strain DSM17938 is no longer available on DSM (as of Feb 2025).

> "We observed an overall reduction in median crying time, regardless of L. reuteri colonization status (n = 14 colonized). There were no differences in E. coli colonization rates or densities, microbial diversity or intestinal inflammation by L. reuteri colonization status. We found that L. reuteri density positively correlated with crying time, and E. coli density negatively correlated with microbial diversity. As density of L. reuteri was associated with increased crying time, L. reuteri supplementation may not be an appropriate treatment for all infants with colic... These divergent findings may reflect differences in geographic location of previous studies (Italy, Poland, China and Canada) and rates of exclusive breastfeeding (majority of infants in previous studies versus 35% in our study)...In infants colonized by L. reuteri, we observed an unexpected positive relationship between L. reuteri density and crying time. Although this finding may generate concerns that high density of L. reuteri could exacerbate symptoms of colic, there does not appear to be a link with gut inflammation as there was no association between L. reuteri density and calprotectin." ([Nation et al 2017](https://pmc.ncbi.nlm.nih.gov/articles/PMC5678104/))

### use as probiotic for gut health ###
Several strains appear to be used in the context of gut health. [Lactobacillus rhamnosus GG](https://www.atcc.org/products/baa-3227) is reclassified as *Lacticaseibacillus rhamnosus* in [Zheng 2020](https://doi.org/10.1099/ijsem.0.004107). Some mechanisms of action as a probiotic are reviewed by [Segers 2014](https://pmc.ncbi.nlm.nih.gov/articles/PMC4155824/). Early use of *L. rhamnosus GG* dissolved in milk was found for treatment of [Clostridium difficile](https://en.wikipedia.org/wiki/Clostridioides_difficile) by [Gorbach 1987](https://pubmed.ncbi.nlm.nih.gov/2892070/), the researchers who isolated the original GG strain.

> "The patients had had 2-5 relapses over a 2-10 month period. Each relapse was associated with diarrhoea, abdominal pain, and weight loss... Lactobacillus GG was given as a concentrate in 5 ml of skim milk, at a daily dose of 10^11 viable bacteria, for 7-10 days... Four patients (A, B, C, and E) had an immediate satisfactory result, with termination of diarrhoea, no further relapses, and negative, or very low, toxin titres in the stool." ([Gorbach 1987](https://pubmed.ncbi.nlm.nih.gov/2892070/))

In another study, patients took oral probiotics for [atopic eczema](https://en.wikipedia.org/wiki/Atopic_dermatitis) by [Kalliomaki 2001](https://pubmed.ncbi.nlm.nih.gov/11297958/). The same strain was used against rotavirus infection in a hospital setting [Szajewska 2001](https://pubmed.ncbi.nlm.nih.gov/11241043/), though with few patients; only 15 of 81 recruited children experienced diarrhea.

Review of the varied roles of *L. gasseri* by [Selle and Klaenhammer 2013](https://doi.org/10.1111/1574-6976.12021), also mentions use in gut health, against H. pylori [Ushiyama 2003](https://pubmed.ncbi.nlm.nih.gov/12859730/). One strain L. gasseri LM19 was identified from human milk, implicating its role in health of both mother and baby. ([Garcia-Gutierrez 2020](https://pmc.ncbi.nlm.nih.gov/articles/PMC7162838/)) They noted "Well-diffusion assay only inhibited the growth of L. bulgaricus. As L. bulgaricus was the most sensitive indicator, it was used in subsequent tests.", though it is unclear why this species was used at all, or what it would mean that two Lactobacillus species could inhibit each other.

In a study on potential probiotics against C. albicans and Gardnerella, strain L. gasseri VHProbi E09 had inhibition zones against both species in vitro ([Zhang 2024](https://pmc.ncbi.nlm.nih.gov/articles/PMC11194266/)). The species was assigned through a rather incomplete tree to L. gasseri based on [L. gasseri GCF000014425](https://www.ncbi.nlm.nih.gov/datasets/genome/GCF_000014425.1/), and may be better assigned to another species. Another strain Lactobacillus gasseri LM1065 was shown to be fungistatic against C. albicans. ([Bae 2023](https://pmc.ncbi.nlm.nih.gov/articles/PMC10374649/))

Again, a L. crispatus strain is used in gut health, Lactobacillus crispatus KT-11, to demonstrate the in vitro inhibition of rotaviruses by S-layer proteins, potentially similar to the [one from L. crispatus](https://www.ncbi.nlm.nih.gov/protein/CAA07708.1). [Kawahara 2022](https://pmc.ncbi.nlm.nih.gov/articles/PMC8902352/) They state:

> "DS-1 infection was significantly suppressed by pre-infection treatment with KT-11 SLP in a concentration-dependent manner. Conversely, KT-11 SLP did not suppress the infection of the Wa strain even after pre-infection treatment at 100 μg/mL." ([Kawahara 2022](https://pmc.ncbi.nlm.nih.gov/articles/PMC8902352/))

### hydrogen peroxide ###
It is repeated reported that peroxide is generated by lactic acid bacteria to protect the host, first through epidemiological studies, then through in vitro studies. As the vagina is typically hypoxic, it is unlikely that peroxide is produced in meaningful quantities. Nonetheless, this was reported as being an important feature of these bacteria for some time:

> "Bactericidal activity. The reaction mixture containing the components indicated in a total volume of 0.5 ml was incubated in 12 X 75 mm polystyrene test tubes (Falcon no. 2054; Becton Dickinson, Oxnard, CA) in a shaking water bath for 60 min at 37°C. A 0.1-ml aliquot of the reaction mixture was serially diluted to 0.1 M sodium sulfate, and the viable cell count was determined by the pour plate method using trypticase-soy agar or L. acidophilus alone and as surface colonies on HBT agar plates for G. vaginalis alone and when a combination of L. acidophilus and G. vaginalis was used. The Gardnerella and Lactobacillus colonies on HBT plates could be distinguished by colony morphology and (3 hemolysis. In experiments with B. bivius, incubations were for 30 min in air, after which serial dilutions were made in 0.9%NaCl/0.02% dithiothreitol and the viable cell count determined as surface colonies after anaerobic growth on Brucella agar." ... "The components of the LB+ -peroxidase-halide system are in the vagina in amounts adequate for the production of a bactericidal effect." ([Klebanoff 1991](https://pubmed.ncbi.nlm.nih.gov/1647428/))

> "The predominant lactobacilli were the species Lactobacillus crispatus (38%) and L. jensenii (41%). Of 57 women initially colonized by H2O2-producing L. crispatus or L. jensenii 23 (40%) remained colonized over 8 months, compared with 1 (5%) of 21 women colonized by other H2O2-producing species or by H2O2-negative strains (P=.01). Frequency of sexual intercourse (⩾1 sex act per week) was associated with loss of colonization with H2O2-producing lactobacilli (P=.018), as was antibiotic use (P⩽.0001). Other behavioral and demographic characteristics did not predict sustained colonization. The production of H2O2 is closely linked with species and is a predictor for sustained long-term colonization of the vagina" ([Vallor et al 2001](https://doi.org/10.1086/324445))

> "Lactobacillus crispatus is one of the predominant hydrogen peroxide (H2O2)-producing species found in the vagina and is under development as a probiotic for the treatment of bacterial vaginosis." ([Antonio 2003](https://doi.org/10.1128%2FJCM.41.5.1881-1887.2003))

> "Hydrogen peroxide production by vaginal lactobacilli represents one of the most important defense mechanisms against vaginal colonization by undesirable microorganisms. To quantify the ability of a collection of 45 vaginal Lactobacillus strains to generate H2O2, we first compared three published colorimetric methods. It was found that the use of DA-64 as a substrate rendered the highest sensitivity, while tetramethyl-benzidine (TMB) maintained its linearity from nanomolar to millimolar H2O2 concentrations. Generation of H2O2 was found to be especially common and strong for L. jensenii strains, while it was variable among L. crispatus and L. gasseri strains. Biosynthesis of H2O2 only occurred upon agitation of the cultures, but the H2O2-producing machinery was already present in them before aeration started." ([Martin and Suarez 2010](https://doi.org/10.1128%2FAEM.01631-09))

> "Compared with the rhythm and IUD groups, the condom group showed greater colonization of L. crispatus, which usually produce H2O2." ([Ma 2013](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3720897/))

> "In our study, the lactobacilli strains isolated from healthy women produced significantly higher amounts of hydrogen peroxide compared to strains of infertile couple's women. It has been shown earlier that H2O2-producing lactobacilli are present in the vagina of most healthy women but are absent in most women with bacterial vaginosis. In our study, 108 Lactobacillus strains out of 135 produced hydrogen peroxide. The majority of L. crispatus and L. jensenii, and less than half of L. gasseri strains produced hydrogen peroxide." ([Hutt 2016](https://pmc.ncbi.nlm.nih.gov/articles/PMC4985617/))

> "Notably, the VM is frequently dominated by L. crispatus, commonly associated with a healthy status of the vaginal tract by protecting the host from viral, bacterial, and fungal infections due to the production of various antimicrobial compounds, such as hydrogen peroxide, lactic acid, and bacteriocin-like products" ([Mancabelli 2021](https://journals.asm.org/doi/full/10.1128/aem.02899-20))

One study presents it two ways, so the authors cannot make up their minds about whether to use peroxide or not.

> "Moreover, Lactobacillus production of hydrogen peroxide as diffusible inhibitory substances could be connected to antimicrobial properties of the vaginal microbiota, representing an important nonspecific antimicrobial defense mechanism due to a highly toxic state (Kullisaar et al., 2002; Mijac et al., 2006)... However, hydrogen peroxide production by Lactobacilli as well as L. gasseri 1A‐TV and L. crispatus 35A‐TV can be considered an additional beneficial effect for vaginal health (Antonio et al., 2005; Pendharkar et al., 2013), in vivo, conversely, under microaerobic (hypoxic) conditions such as the cervicovaginal environment, the concentration of H2O2 produced does not achieve the amount necessary to have antimicrobial activity in the vaginal environment. The low vaginal O2 levels measured in in vivo studies have been associated with little or no H2O2 in the hypoxic cervicovaginal environment (O'Hanlon et al., 2011). Therefore, these findings support an important role of lactic acids as main products at high concentrations in a hypoxic environment such as the vagina. However, H2O2 production remains an in vitro marker for beneficial vaginal properties (Tachedjian et al., 2018)." ([Scillato et al 2021](https://pmc.ncbi.nlm.nih.gov/articles/PMC8483400/))

Fortunately, several papers report experiments that argue against the role of hydrogen peroxide, nearly all by the same author:

> "Results showed that H2O2 was liberated especially by: Lactobacillus delbrueckii, Lactobacillus acidophilus, Lactobacillus crispatus, Lactobacillus johnsonii and L. gasseri. Hydrogen peroxide reached concentrations from 0.05 to 1.0 mM, which under intensive aeration increased even up to 1.8 mM. Microorganisms related to vaginal pathologies show varied resistance to the action of pure H2O2. Most potent inhibitory activity against bacteria and yeasts was presented by Lactobacillus culture supernate producing H2O2, followed by the nonproducing strain and pure H2O2. To conclude - the antimicrobial activity of lactobacilli is a summation of various inhibitory mechanisms in which H2O2 plays some but not a crucial role, in addition to other substances." ([Strus et al 2006](https://doi.org/10.1111/j.1574-695x.2006.00120.x))

> "The mean H2O2 measured in fully aerobic CVF was 23 ± 5 μM; however, 50 μM H2O2 in salt solution showed no in vitro inactivation of HSV-2, Neisseria gonorrhoeae, Hemophilus ducreyii, or any of six BV-associated bacteria. CVF reduced 1 mM added H2O2 to an undetectable level, while semen reduced 10 mM added H2O2 to undetectable. Moreover, the addition of just 1% CVF supernatant abolished in vitro pathogen-inactivation by H2O2-producing lactobacilli. Given the H2O2-blocking activity of CVF and semen, it is implausible that H2O2-production by vaginal lactobacilli is a significant mechanism of protection in vivo." ([O'Hanlon et al 2010](https://doi.org/10.1186%2F1471-2334-10-120))

> "Under optimal, anaerobic growth conditions, physiological concentrations of lactic acid inactivated BV-associated bacteria without affecting vaginal lactobacilli, whereas physiological concentrations of H2O2 produced no detectable inactivation of either BV-associated bacteria or vaginal lactobacilli. Moreover, at very high concentrations, H2O2 was more toxic to vaginal lactobacilli than to BV-associated bacteria. On the basis of these in vitro observations, we conclude that lactic acid, not H2O2, is likely to suppress BV-associated bacteria in vivo." ([O'Hanlon et al 2011](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3161885/))

> "In the cervicovaginal environment, the production of hydrogen peroxide (H2O2) by vaginal Lactobacillus spp. is often mentioned as a critical factor to the in vivo vaginal microbiota antimicrobial properties. We present several lines of evidence that support the implausibility of H2O2 as an "in vivo" contributor to the cervicovaginal milieu antimicrobial properties. An alternative explanation is proposed, supported by previous reports ascribing protective and antimicrobial properties to other factors produced by Lactobacillus spp. capable of generating H2O2. Under this proposal, lactic acid rather than H2O2 plays an important role in the antimicrobial properties of protective vaginal Lactobacillus spp." ([Tachedjian, O'Hanlon, and Ravel 2018](https://doi.org/10.1186/s40168-018-0418-3))







