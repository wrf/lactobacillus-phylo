#!/usr/bin/env python
#
# filter_and_rename_genbank_prots.py

"""filter_and_rename_genbank_prots.py  last modified 2022-11-02

filter_and_rename_genbank_prots.py -n lactobacillus_assembly_to_id.tab ncbi*/*.faa.gz 2> species_stats.tab

filter_and_rename_genbank_prots.py -n candida_assembly_to_id.tab prot-fasta-2022-07-19/*.faa.gz
"""

import sys
import re
import gzip
import time
import argparse
from collections import defaultdict,Counter
from Bio import SeqIO

def read_all_prot_fasta_gz(fastafilelist, topleveldict, print_stats, is_verbose):
	'''read all fasta files, get some stats'''

	assembly_id_dict = {} # key is assembly ID, like ASM1198v1, value is True

	filename_to_ids = defaultdict(list) # list of all IDs in each file
	filename_to_genus = {} # key is filename, value is dict where species name is key, value is count, there should be only 1 value

	unique_accessions = defaultdict(int) # key is accession, first split, value is count of unique accessions per species
	final_prot_name = defaultdict(int) # key is prot ID, value is count

	# genera to always append assembly ID
	flagged_genera = ["Lactobacillus", "Lactobacillus_sp"]

	for fastafilename in fastafilelist:
		genussp_counts = defaultdict(int) # key is genus-species construction, value is count

		try:
			assembly_id = re.search("\.\d_(.*)_protein\.faa", fastafilename).group(1)
		except AttributeError:
			print( "ERROR: cannot parse assembly ID from {}".format(fastafilename) , file=sys.stderr)
		if assembly_id in assembly_id_dict:
			print( "WARNING: duplicate assembly ID {} from {}".format(assembly_id, fastafilename), file=sys.stderr)
			#continue
		else:
			assembly_id_dict[assembly_id] = True

		# check for gzip
		if fastafilename.rsplit('.',1)[-1]=="gz": # autodetect gzip format
			_opentype = gzip.open
			sys.stderr.write("# Opening {} as gzipped\n".format(fastafilename))
		else: # otherwise assume normal open for fasta format
			_opentype = open
			sys.stderr.write("# Opening {}\n".format(fastafilename))

		outfilename = "{}.renamed.faa".format( fastafilename.rsplit('.',2)[0] )

		protcounter = 0 # reset each file
		# iterate seqs
		with _opentype(fastafilename,'rt') as fa, open(outfilename,'w') as rp:
			for seqrec in SeqIO.parse(fa,"fasta"):
				protcounter += 1
				seqid = seqrec.id
				seqdesc = seqrec.description # usually full line, unparsed

				filename_to_ids[fastafilename].append(seqid)
				unique_accessions[seqid] += 1

				genusspecies = topleveldict.get(assembly_id,None)
				if genusspecies is None:
					if is_verbose: # assuming all should have top level assignments
						print( "WARNING: cannot find genus for assembly ID {} in {}".format(assembly_id, fastafilename), file=sys.stderr)

					# try to infer genus-species from the protein fasta itself
					genusspecies = re.search(".*\[(.+)\]$", seqdesc).group(1).replace("Candidatus ","")
					genusspecies = genusspecies.replace("sp.","sp") # replace empty species names
					genusspecies = genusspecies.replace("#","-") # for cases like DBVPG#7215
					for repitem in ["(", ")", "[", "]", ":", "."]: # : from PREDICTED:_something
						genusspecies = genusspecies.replace(repitem,"") # these seem to cause trouble downstream
					genusspecies = genusspecies.replace(" ","_")

				genussp_counts[genusspecies] += 1

				new_seqid = "{}|{}\n".format( genusspecies, protcounter )
				final_prot_name[new_seqid] += 1
				seqrec.id = new_seqid
				seqrec.description = seqid
				rp.write( seqrec.format("fasta") )
		filename_to_genus[fastafilename] = dict(genussp_counts)

		if print_stats:
			unique_accession_count = 0
			nonunique_count = 0
			for ac in filename_to_ids.get(fastafilename,{}):
				ac_count = unique_accessions.get(ac,0)
				if ac_count == 1:
					unique_accession_count += 1
				elif ac_count > 1:
					nonunique_count += 1

			stats_line = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format( fastafilename, assembly_id, genusspecies, len(genussp_counts), len(filename_to_ids.get(fastafilename,{})), protcounter, unique_accession_count, nonunique_count )
			sys.stderr.write(stats_line)
	sys.stderr.write("# Found {} unique accessions\n".format( len(unique_accessions) ) )
	if final_prot_name: # list any genus-species names that occur more than once
		repeat_prot_name_count = defaultdict(int) # key is species, value is count
		for seqid, p_count in final_prot_name.items():
			if p_count > 1: # count of that protein name across all files, if more than 1 is a problem
				genusspecies = seqid.split("|",1)[0]
				repeat_prot_name_count[genusspecies] += 1
		if repeat_prot_name_count: # if any species had proteins with duplicate names
			for genusspecies in repeat_prot_name_count.keys():
				sys.stderr.write("# non-unique name for species {} used\n".format( genusspecies ) )

def get_top_level_names(namesfile):
	species_name_dict = {}
	sys.stderr.write("# Reading definitive names from {}\n".format(namesfile) )
	for line in open(namesfile,'r'):
		lsplits = line.rstrip().split("\t")
		species_name_dict[ lsplits[0] ] = lsplits[1].replace(" ","_")
	sys.stderr.write("# Found {} entries\n".format( len(species_name_dict) ) )
	return species_name_dict

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_prots', nargs="*", help="fasta format files, can be .gz")
	parser.add_argument('-n','--names', help="tabular txt file, of ASM ID  name")
	parser.add_argument('-s','--fasta-stats', action="store_true", help="print stats about each fasta file to stderr")
	parser.add_argument('-v','--verbose', action="store_true", help="give more info, including timestamp")
	args = parser.parse_args(argv)

	# use defaults, some have different names for nuclear and mitochondrial proteins in the RefSeq fasta files
	species_name_dict = {"ASM252v1":"Yarrowia_lipolytica_CLIB122", "ASM254v2":"Candida_glabrata_CBS_138",  "ASM644v2":"Debaryomyces_hansenii_CBS767", "ASM14955v1":"Fusarium_verticillioides_7600"}
	if args.names: # otherwise take all names from genome assembly
		species_name_dict = get_top_level_names(args.names)
	read_all_prot_fasta_gz(args.input_prots, species_name_dict, args.fasta_stats, args.verbose)



if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
